/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author i110300
 */
@Entity
public class IncomeType implements Serializable {
    @Id
    @GeneratedValue
    int Id;
    
    private String descricao;

    public IncomeType() {
    }

    public IncomeType(String desc) {
        this.descricao = desc;
    }

    public boolean compare(IncomeType typeIncome) {
        if (descricao.equals(typeIncome.descricao)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return String.format(descricao);
    }
}
