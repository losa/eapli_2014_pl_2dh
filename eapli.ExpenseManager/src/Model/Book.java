/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.Date;
import persistence.inmemory.MonthPageRepository;

/**
 *
 * @author Grupo8
 */
public class Book {
    
    private int bookID;
    private float initBalance = 0;

    public Book() {}
    
    public Book(int bookID, float initBalance) {
        this.bookID = bookID;
        this.initBalance = initBalance;
    }

    //MonthPage monthPage;
    MonthPageRepository monthPageRep = new MonthPageRepository();
   // ArrayList<MonthPage>pagMensais=new ArrayList<MonthPage>();

    /**
     * Loads the monthPage that refers to the year and month of a given date.
     *
     * @param date required monthPage's date
     * @return returns the required monthPage
     */
    public MonthPage loadMonthPage(Date date) {
        MonthPageRepository monthPageRepository = Factories.RepositorioFactory.getMonthPageRepository();
        MonthPage monthPage = monthPageRepository.loadMonthPage(date);
        return monthPage;
    }

    /**
     * @param start the start date of the time interval from witch the
     * MonthPages will be loaded
     * @param end the end date of the time interval from witch the MonthPages
     * will be loaded
     * @return returns an ArrayList of MonthPages
     */
    public ArrayList<MonthPage> loadMonthPages(Date start, Date end) {
        MonthPageRepository monthPageRepository = Factories.RepositorioFactory.getMonthPageRepository();
        return monthPageRepository.loadMonthPage(start, end);
    }

    /**
     * Persists a given monthPage
     *
     * @param monthPage the monthPage that is to be persisted
     */
    public void saveMonthPage(MonthPage monthPage) {
        MonthPageRepository monthPageRepository = Factories.RepositorioFactory.getMonthPageRepository();
        monthPageRepository.saveMonthPage(monthPage);
        //this.monthPage=monthPage;
    }

    /**
     * Registers and saves the Income
     *
     * @param incomeValue nominal value of the Income
     * @param description description of the Income
     * @param date date in which the Income was received
     * @param typeIncome type of the Income
     * @return the monthPage that was loaded
     */
    public MonthPage registerIncome(float incomeValue, String description, Date date, IncomeType typeIncome) {
        MonthPage monthPage = loadMonthPage(date);
        monthPage.registerIncome(incomeValue, description, date, typeIncome);
        return monthPage;
    }

    
    public MonthPage registerExpense(String description, Date date, float price, TipoDespesa typeExpense) {
        Expense expense = new Expense(date, price, description, typeExpense);
        MonthPage monthPage = loadMonthPage(date);
         if(monthPage==null){
            System.out.println("Não existe essa monthpage");   
        }
        else{
        monthPage.registerExpense(expense);
         } return monthPage;
    }
    
    /**
     * Saves an existing Income into the MonthPage
     *
     * @param income income that is to be saved into the monthPage
     */
    public MonthPage addIncome(Income income) {
        MonthPage monthPage = loadMonthPage(income.getDate());
        monthPage.addIncome(income);
        return monthPage;
    }

//     public ArrayList<MonthPage> getListMonthPages() {
//        return pagMensais;
//    }
//    
//    public boolean addMonthPage(MonthPage mp)
//    {
//       return pagMensais.add(mp);
//    }
    public ArrayList<MonthPage> getListMonthPages() {
        return monthPageRep.getMonthPages();
    }

    public boolean addMonthPage(MonthPage mp) {
        return monthPageRep.registerMonthPage(mp);//getMonthPages().add(mp);
    }

    public MonthPageRepository getMonthPageRepository() {
        return monthPageRep;
    }

    public float getAnualExpenses(int ano) {
        float total = 0;
        int aux;
        for (MonthPage mp : getListMonthPages()) {
            aux = mp.getYear();
            if (ano == aux) {
                total += mp.calculateTotalExpense();;
            }
        }
        return total;
    }

    public int CountYearMonths(int ano) {
        int meses = 0;
        int aux;
        for (MonthPage mp : getListMonthPages()) {
            aux = mp.getYear();
            if (ano == aux) {
                meses++;
            }
        }
        return meses;
    }
}
