/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Hugo Costa
 */
@Entity
public class TipoDespesa implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int Id;
    
    private String tipo; 

    public TipoDespesa(String tipo) {
    this.tipo = tipo; 
    }
    
    public TipoDespesa() {
     }

    @Override
    public String toString() {
        return getTipo();
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }
    
    
    
}
