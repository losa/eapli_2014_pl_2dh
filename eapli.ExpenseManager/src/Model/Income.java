/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author Grupo8
 */
@Entity
public class Income implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private float incomeValue;
    private String description;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;
    private IncomeType typeIncome;
    
    public Income(){}
    
    public Income(float incomeValue, String description, Date date, IncomeType typeIncome) {
        this.incomeValue = incomeValue;
        this.description = description;
        this.date = date;
        this.typeIncome = typeIncome;
    }

    /**
     * @return the incomeValue
     */
    public float getIncomeValue() {
        return incomeValue;
    }

    /**
     * @param incomeValue the incomeValue to set
     */
    public void setIncomeValue(float incomeValue) {
        this.incomeValue = incomeValue;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the typeIncome
     */
    public IncomeType getTypeIncome() {
        return typeIncome;
    }

    /**
     * @param typeIncome the typeIncome to set
     */
    public void setTypeIncome(IncomeType typeIncome) {
        this.typeIncome = typeIncome;
    }

     /**
     * @param start the start date of a set time interval (Note that the day is ignored)
     * @param end the end date of a set time interval (Note that the day is ignored)
     * @return returns true if this income line is inside the defined time interval
     */
     public boolean Datebetween(Date start,Date end){
        //testado e a funcionar corretamente
        Calendar start_c = Calendar.getInstance();
        Calendar end_c =Calendar.getInstance();
        start_c.setTimeInMillis(start.getTime());
        end_c.setTimeInMillis(end.getTime());
        Calendar aux=Calendar.getInstance();
        aux.setTimeInMillis(date.getTime());
        if(start_c.getTimeInMillis()<=aux.getTimeInMillis() && aux.getTimeInMillis()<=end_c.getTimeInMillis()){
        return true;
        }else{return false;}
    }
   
}
