/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import javax.persistence.Entity;

/**
 *
 * @author Zé
 */
@Entity
public class Cheque extends MeanPayment {

    private String bank;
    private int num = -1;

    public Cheque() {

    }

    public Cheque(int num, String desc, String bank) {
        super();
        typeID = 4;
        description = desc;
        this.num = num;
        this.bank = bank;
    }

    public String getBank() {
        return bank;
    }

    public int getNum() {
        return num;
    }
}