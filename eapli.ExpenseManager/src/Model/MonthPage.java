/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.xml.crypto.Data;
import persistence.inmemory.RepositorioLivro;

/**
 *
 * @author ASUS
 */
@Entity
public class MonthPage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int Id;
    private int year;
    private int month;
    private float saldo;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    //transient é para ignorar o campo
    //@Transient
    private ArrayList<Expense> expenses = new ArrayList<Expense>();
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    //@Transient
    private ArrayList<Income> incomes = new ArrayList<Income>();

    
    public MonthPage() {
    }
        
    public MonthPage(int year, int month, float saldo) {
        this.month = month;
        this.year = year;
        this.saldo = saldo;
    }    

    public MonthPage(int year, int month) {
        this.month = month;
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }
    
    public ArrayList<Income> getIncomes(){
        return incomes;
    }

    public void actualizarSaldo() {
        float totalDespesa = 0;
        float totalRendimento = 0;
        for (Expense x : getExpenses()) {
            totalDespesa = totalDespesa + x.getPrice();
        }
        for (Income x : getIncomes()) {
            totalRendimento = totalRendimento + x.getIncomeValue();
        }
        //getSaldo().setSaldo(totalRendimento - totalDespesa);
        setSaldo(totalRendimento - totalDespesa);
    }

    public void registerIncome(Income income) {
        getIncomes().add(income);
        actualizarSaldo();
        persistence.jpa.RepositorioIncomeJPA gravaJPA=new persistence.jpa.RepositorioIncomeJPA();
        gravaJPA.save(income);
    }
    
    //Comentado(Teste) --------------------------------------------------------------------------------------------
    /**
     * Registers and saves the Income
     * @param incomeValue nominal value of the Income
     * @param description description of the Income
     * @param date date in which the Income was received
     * @param typeIncome type of the Income
     */
    public void registerIncome(float incomeValue, String description, Date date, IncomeType typeIncome) {
        Income income = new Income(incomeValue, description, date, typeIncome);
        addIncome(income);
    }

    /**
     * Saves an existing Income into the MonthPage
     * @param income income that is to be saved into the monthPage
     */
    public void addIncome(Income income) {
        getIncomes().add(income);
        actualizarSaldo();
        persistence.jpa.RepositorioIncomeJPA gravaJPA=new persistence.jpa.RepositorioIncomeJPA();
        gravaJPA.save(income);
    }
    
    //--------------------------------------------------------------------------------------------------------------

    /**
     * Adds a expense line to the expense list
     * @param expense a expense
     */
    public void registerExpense(Expense expense) {
        getExpenses().add(expense);
        persistence.jpa.RepositoryExpenseJPA despesa= new persistence.jpa.RepositoryExpenseJPA();
        despesa.save(expense);
        
    }

    
    
    /**
     * @param date date to test if the page belongs to that month and year
     */
    public boolean verifyMonthPageDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
        if (getMonth() == calendar.get(Calendar.MONTH) && getYear() == calendar.get(Calendar.YEAR)) {
            return true;
        } else {
            return false;
        }
    }
    
     /**
     * @param start the start date of a set time interval (Note that the day is ignored)
     * @param end the end date of a set time interval (Note that the day is ignored)
     * @return returns true if this month page is inside the defined time interval
     */
    public boolean Datebetween(Date start,Date end){
        //testado e a funcionar corretamente
        Calendar start_c = Calendar.getInstance();
        Calendar end_c =Calendar.getInstance();
        start_c.setTimeInMillis(start.getTime());
        end_c.setTimeInMillis(end.getTime());
        Calendar aux=Calendar.getInstance();
        aux.setTimeInMillis((new Date(year, month,1)).getTime());
        if(start_c.getTimeInMillis()<=aux.getTimeInMillis() && aux.getTimeInMillis()<=end_c.getTimeInMillis()){
        return true;
        }else{return false;}
    }
    

    public boolean compare(MonthPage monthPage) {
        if (getMonth() == monthPage.getMonth() && getYear() == monthPage.getYear()) {
            return true;
        } else {
            return false;
        }
    }

    public double calculateTotalIncome() {
        double sum = 0;
        for (Income inc : getIncomes()) {
            sum += inc.getIncomeValue();
        }
        return sum;
    }
/**
 * 
 * @return returns the total expenses of the monthpage
 */
      public double calculateTotalExpense() {
        double sum = 0;
        for (Expense ex : expenses) {
            sum += ex.getPrice();
        }
        return sum;
    }
    
    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @param month the month to set
     */
    public void setMonth(int month) {
        this.month = month;
    }

    /**
     * @return the saldo
     */
    public float getSaldo() {
        return saldo;
    }

    /**
     * @param saldo the saldo to set
     */
    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    /**
     * @return the despesas
     */
    public ArrayList<Expense> getExpenses() {
        return expenses;
    }

    /**
     * @param despesas the despesas to set
     */
    public void setExpenses(ArrayList<Expense> despesas) {
        this.expenses = despesas;
    }

    /**
     * @param incomes the incomes to set
     */
    public void setIncomes(ArrayList<Income> incomes) {
        this.incomes = incomes;
    }
    
    
    
      public ArrayList GastosPorTipodeDespesa (){
        String str=""; int aux;
        
        ArrayList<String> tipodespesa = new ArrayList(); 
        ArrayList<Expense> totalexpense = new ArrayList(); 
        
          for (int i = 0; i < expenses.size(); i++) {
              String despesa= expenses.get(i).gettypeExpense().getTipo(); 
              int p=0;   
              for (int j = 0; j < tipodespesa.size(); j++) {
                    if (tipodespesa.get(i).equalsIgnoreCase(despesa)) {
                        p=1; 
                    }
                    
                    if (p!=1) {
                      tipodespesa.add(despesa); 
                  }
              }
              
          }
        
        
          for (int i = 0; i < tipodespesa.size(); i++) {
              String p= tipodespesa.get(i); 
              float valor =0; 
              for (int j = 0; j < expenses.size(); j++) {
                  if (p==expenses.get(i).gettypeExpense().getTipo()) {
                     valor+=expenses.get(i).getPrice(); 
                  }
              }
              
            
              Date data = new Date(1,month, year); 
              Expense despesa = new Expense(data, valor, new TipoDespesa(p)); 
              
             totalexpense.add(despesa); 
          }          
        return totalexpense;
        
      }
}
      
        
       
      
        

