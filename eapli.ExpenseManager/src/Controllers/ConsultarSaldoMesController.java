/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.MonthPage;
import persistence.inmemory.RepositorioLivro;
import java.util.Date;

/**
 *
 * @author José
 */
public class ConsultarSaldoMesController {
    public float saldoMes;
   public ConsultarSaldoMesController(Date data){
       RepositorioLivro repoLivro=Factories.RepositorioFactory.getBookRepository();
       MonthPage pagMes=repoLivro.loadBook().loadMonthPage(data);
       //saldoMes=pagMes.getSaldo().getSaldo();
       saldoMes=pagMes.getSaldo();
   }

}
