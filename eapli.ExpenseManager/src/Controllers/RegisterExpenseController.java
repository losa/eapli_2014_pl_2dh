/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.Book;
import Model.Expense;
import Model.IncomeType;
import Model.MonthPage;
import Model.TipoDespesa;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import persistence.jpa.RepositorioTipoDespesaJPA;

/**
 *
 * @author ASUS
 */
public class RegisterExpenseController {
    public void registerExpense(String description, Date date, float price, TipoDespesa typeExpense) {
        Expense expense = new Expense(date, price, description, typeExpense);
        
        persistence.inmemory.RepositorioLivro repositorioLivro = Factories.RepositorioFactory.getBookRepository();
        Book book = repositorioLivro.loadBook();
        
        MonthPage monthPage = book.loadMonthPage(date);
        if(monthPage==null){
            System.out.println("Não existe essa monthpage");
            
        }
        else{
        monthPage.registerExpense(expense);
        
        book.saveMonthPage(monthPage);
        }
    }
    
        public void registerExpense(Expense expense) {
        
        persistence.inmemory.RepositorioLivro repositorioLivro = Factories.RepositorioFactory.getBookRepository();
        Book book = repositorioLivro.loadBook();
        
        MonthPage monthPage = book.loadMonthPage(expense.getDate());
        
        monthPage.registerExpense(expense);
        
        book.saveMonthPage(monthPage);
    }
    
        public List<TipoDespesa> loadListExpenseTypes() {
//        persistence.inmemory.RepositorioTipoDespesa repositorioTipoDespesa=Factories.RepositorioFactory.getRepositorioTipoDespesa();
//        return repositorioTipoDespesa.loadAll();
          persistence.jpa.RepositorioTipoDespesaJPA repositorioTipoDespesa =new persistence.jpa.RepositorioTipoDespesaJPA();
          return repositorioTipoDespesa.loadAll();
    }
}
