/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Model.Book;
import Model.Expense;
import Model.MonthPage;
import Model.TipoDespesa;
import java.util.ArrayList;
import java.util.Date;
import persistence.inmemory.RepositorioLivro;

/**
 *  Class encarregada de calcular as tendências de gastos mensais do utilizador.
 * @author 1120034 José Mendes
 */
public class MonthlyExpensesTendenciesController {
    
    private Book book;
    private ArrayList<ArrayList<Expense>> Meses;
    private ArrayList<Expense> acumulacaoMensal;
    public MonthlyExpensesTendenciesController(){
        book=RepositorioLivro.loadBook();
    }
    public String CalculateTendencies(){
        int ano = 0, aux; String str="";
        for(MonthPage mp : book.getListMonthPages()){
            aux=mp.getExpenses().get(ano).getDate().getYear();
            if(ano != aux){
                ano=aux;
                str+="\nTotal "+(ano+1900)+"€ - "+book.getAnualExpenses(ano+1900)+" || "+anualTendencies(ano+1900);
                System.out.println(str);
            }
            float percentagem=(float)mp.calculateTotalExpense()/book.getAnualExpenses(mp.getYear());
            str+="\n\t"+mp.getMonth()+" "+mp.calculateTotalExpense()+"€ "
                    +(percentagem-100.0/book.CountYearMonths(ano))
                    +"% || "+MonthTendencies(mp);
        }
        return str;
    }
    public String MonthTendencies(MonthPage mp){
        String str=""; int total;
        ArrayList<Expense> pattern = new ArrayList<>();
        
        for(Expense exp : mp.getExpenses()){
            if(pattern.contains(exp)){
                float pr=pattern.get(pattern.indexOf(exp)).getPrice();
                float pr2=pattern.get(pattern.indexOf(exp)).getPrice();
                pattern.get(pattern.indexOf(exp)).setPrice(pr+pr2);
            }
            else{
                pattern.add(new Expense(new Date(mp.getYear(),mp.getMonth(),0), exp.getPrice(), exp.gettypeExpense()));
            }
        }
        if(pattern.size()>2){
            total=3;
        }
        else{
            total=pattern.size();
        }
        for(int i = 0; i<total ;i++){
            Expense maior=new Expense();
            maior.setPrice(0);
            for(Expense exp : pattern){
                if(exp.getPrice()>maior.getPrice()){
                    maior=exp;
                }
            }
            float aux_price=maior.getPrice();
            str+=maior.gettypeExpense().getTipo()+" "+aux_price+" "+(aux_price/mp.calculateTotalExpense())*100+"% || ";
            pattern.remove(maior);
        }
        return str;
    }
    public String anualTendencies(int ano){
        String str=""; int aux, total;
        ArrayList<Expense> pattern = new ArrayList<>();
        for(MonthPage mp : book.getListMonthPages()){
            aux=mp.getYear();
            if(ano==aux){
                for(Expense exp : mp.getExpenses()){
                    if(pattern.contains(exp)){
                        float pr=pattern.get(pattern.indexOf(exp)).getPrice();
                        float pr2=pattern.get(pattern.indexOf(exp)).getPrice();
                        pattern.get(pattern.indexOf(exp)).setPrice(pr+pr2);
                    }
                    else{
                        pattern.add(new Expense(new Date(ano,mp.getMonth(),0), exp.getPrice(), exp.gettypeExpense()));
                    }
                }
            }
        }
        float totalAnual = book.getAnualExpenses(ano);
        if(pattern.size()>2){
            total=3;
        }
        else{
            total=pattern.size();
        }
        for(int i = 0; i<total ;i++){
            Expense maior=new Expense();
            maior.setPrice(0);
            for(Expense exp : pattern){
                if(exp.getPrice()>maior.getPrice()){
                    maior=exp;
                }
            }
            float aux_price=maior.getPrice();
            str+=maior.gettypeExpense().getTipo()+" "+aux_price+" "+(aux_price/totalAnual)*100+"% || ";
            pattern.remove(maior);
        }
        System.out.println("\n\t"+str);
        return str;
    }
}
