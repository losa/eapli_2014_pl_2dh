/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.Book;
import Model.Income;
import Model.MonthPage;
import Model.IncomeType;
import java.util.ArrayList;
import java.util.Date;
import persistence.Factory;
import persistence.IRepositoryFactory;

/**
 *
 * @author Grupo8
 */
public class RegisterIncomeController {

    /**
     * Registers and saves the Income
     *
     * @param incomeValue nominal value of the Income
     * @param description description of the Income
     * @param date date in which the Income was received
     * @param typeIncome type of the Income
     */
    public void registerIncome(float incomeValue, String description, Date date, IncomeType typeIncome) {
        persistence.inmemory.RepositorioLivro repositorioLivro = Factories.RepositorioFactory.getBookRepository();
        Book book = repositorioLivro.loadBook();

        MonthPage monthPage = book.registerIncome(incomeValue, description, date, typeIncome);

        book.saveMonthPage(monthPage);
    }

    /**
     * Registers and saves the Income
     *
     * @param income Object income
     */
    public void addIncome(Income income) {

        persistence.inmemory.RepositorioLivro repositorioLivro = Factories.RepositorioFactory.getBookRepository();
        Book book = repositorioLivro.loadBook();

        MonthPage monthPage = book.addIncome(income);

        book.saveMonthPage(monthPage);
    }

    /**
     * Loads all the Income types
     *
     * @return returns an arraylist with all the Income types
     */
    public ArrayList<IncomeType> loadListIncomeTypes() {
        IRepositoryFactory repositoryFactory = Factory.getInstance().getRepositoryFactory();
        return repositoryFactory.getIncomeTypeRepository().loadAll();
    }
}
