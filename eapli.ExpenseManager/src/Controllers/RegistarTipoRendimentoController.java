/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.IncomeType;
import java.util.ArrayList;
import persistence.Factory;
import persistence.IRepositoryFactory;

/**
 *
 * @author i110300
 */
public class RegistarTipoRendimentoController {

    public RegistarTipoRendimentoController() {

    }

    public void RegistarTipoRendimento(String nome) {
        IncomeType tr = new IncomeType(nome);
        IRepositoryFactory repositoryFactory = Factory.getInstance().getRepositoryFactory();
        repositoryFactory.getIncomeTypeRepository().save(tr);       
    }
}
