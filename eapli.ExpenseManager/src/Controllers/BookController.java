/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.Book;
import Presentation.InicializarSaldoUI;
import Presentation.MainMenu;
import persistence.inmemory.RepositorioLivro;

/**
 *
 * @author JoaoRodrigues
 */
public class BookController {

    Book _book = RepositorioLivro.loadBook();

    public void validar() {
        if (_book == null) {
            _book = new Book();
        }
        if (_book.getListMonthPages().isEmpty()) {
            InicializarSaldoUI initsaldo = new InicializarSaldoUI(_book);
            initsaldo.show();
        } else {
            MainMenu manu = new MainMenu(_book);
            manu.mainLoop();
        }

    }
}
