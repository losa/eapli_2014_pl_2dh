/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Factories.RepositorioFactory;
import Model.TipoDespesa;
import persistence.Factory;
import persistence.IRepositorioTipoDespesa;
import persistence.IRepositoryFactory;
import persistence.inmemory.RepositorioTipoDespesa;
import persistence.jpa.RepositorioTipoDespesaJPA;

/**
 *
 * @author Hugo Costa
 */


        
public class TipoDespesaController {
    
  public String file = "jpa";
    
  public void registarTipoDespesa(String nome){
              
   TipoDespesa tipoDespesa = new TipoDespesa(nome); 
   tipoDespesa.setTipo(nome); //retirar
   
//   if (file.equals("jpa")){
//   RepositorioTipoDespesaJPA repo=RepositorioFactory.getRepositorioTipoDespesaJPA();
//   repo.save(tipoDespesa);
//   }//retirar
  
    IRepositoryFactory repositoryfactory = Factory.getInstance().getRepositoryFactory();
    IRepositorioTipoDespesa repo = repositoryfactory.getRepositorioTipoDespesa(); 
    repo.save(tipoDespesa);   
  }
   
    
}
