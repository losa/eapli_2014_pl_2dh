/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.Cash;
import Model.Cheque;
import Model.CreditCard;
import Model.DebitCard;
import Model.MeanPayment;
import Model.TypePayment;
import java.util.ArrayList;
import java.util.List;
import persistence.Factory;
import persistence.IRepositoryMeanPay;
import persistence.inmemory.RepoMeansPayment;
import persistence.inmemory.RepoTypesPayment;

/**
 *
 * @author Pedro Amaral
 */
public class MeanPayController {
    private RepoTypesPayment repoTypesPayment;
    private RepoMeansPayment repoMeansPayment;
    private TypePayment typePayment;

    public MeanPayController() {
        
    }
    
    public TypePayment getTypePayment(){
        return typePayment;
    }

    public ArrayList getRepoTypesPayment() {
        return repoTypesPayment.getTypesPayment();
    }
    
    public void createMeansCash() {
        Cash cash = new Cash();
        IRepositoryMeanPay repo = Factory.getInstance().getRepositoryFactory().getRepositoryMeanPay();
        repo.save(cash);
    }

    public void createMeansCredit(int num, String desc, String bank) {
        CreditCard cc = new CreditCard(num, desc, bank);
        IRepositoryMeanPay repo = Factory.getInstance().getRepositoryFactory().getRepositoryMeanPay();
        repo.save(cc);
    }

    public void createMeansDebit(int num, String desc, String bank) {
        DebitCard dc = new DebitCard(num, desc, bank);
        IRepositoryMeanPay repo = Factory.getInstance().getRepositoryFactory().getRepositoryMeanPay();
        repo.save(dc);
    }

    public void createMeansCheque(int num, String desc, String bank) {
        Cheque cheque = new Cheque(num, desc, bank);
        IRepositoryMeanPay repo = Factory.getInstance().getRepositoryFactory().getRepositoryMeanPay();
        repo.save(cheque);
    }
    
    public List<MeanPayment> ListAllMeans() {
        IRepositoryMeanPay repo = Factory.getInstance().getRepositoryFactory().getRepositoryMeanPay();
        return repo.loadAll();
    }
}