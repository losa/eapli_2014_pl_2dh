/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Model.Book;
import Model.TipoDespesa;
import persistence.inmemory.MonthPageRepository;
import Factories.RepositorioFactory;
import Model.Expense;
import Model.IncomeType;
//import Model.Book;
//import Model.Despesa;
import persistence.inmemory.RepositorioTipoDespesa;
import eapli.util.Console;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.xml.crypto.Data;

/**
 *
 * @author Hugo Costa
 */
public class MainMenu extends BaseUI {
// Book a= new Book(); 

    private Book _book;

    public MainMenu(Book book) {
        _book = new Book();
    }

    public void MainMenu() {
    }
    public void mainLoop() {
        
        int opcao;
        VisualizarSaldoUI viSaldo = new VisualizarSaldoUI();
        do {
            System.out.println("\n"+header("MENU")
                    +"\nBalance: " + viSaldo
                    + "\n\t1- Register new Expense"
                    + "\n\t2- Register new Income"
                    + "\n\t3- Register Expense type"
                    + "\n\t4- Show expense type"
                    + "\n\t5- Register Income type"
                    + "\n\t6- Consults"
                    + "\n\t7- Register new Payment Mean"
                    + "\n\t8- Export\\Import Files"
                    +"\n\t0- Exit");

            opcao = Console.readInteger("Choose an option:");

            switch (opcao) {
                case 0:{
                    System.out.println("Shuting Down...");
                    break;
                }
                case 1: {
                    RegisterExpenseUI registerExpenseUI = new  RegisterExpenseUI();
                    registerExpenseUI.show();
                    
                }
                case 2: {
                    RegisterIncomeUI registerIncomeUI = new RegisterIncomeUI();
                    registerIncomeUI.show();
                    break;
                }
                case 3: {
                    RegistarTipoDespesaUI r = new RegistarTipoDespesaUI();
                    r.show();
                    break;
                }

                case 4: {
                    ConsultarTipoDespesaUI consulta = new ConsultarTipoDespesaUI();
                    consulta.display();
                    break;
                }
                case 5: {
                    RegistarTipoRendimentoUI tr = new RegistarTipoRendimentoUI();
                    tr.show();
                    break;
                }
                case 6: {
                    MenuConsultas p = new MenuConsultas();
                    p.mainConsultas();
                    break;
                }
                case 7: {
                    MenuRegMeansPayUI menuregmeanspayui = new MenuRegMeansPayUI();
                    menuregmeanspayui.show();
                    break;
                }
                case 8: {
                new FileExportImportUI();
                break;
                }
                default: {
                    System.out.println("Opção errada");
                }
            }

        } while (opcao != 0);
    }

    @Override
    public String show() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
