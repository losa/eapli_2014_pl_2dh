/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Presentation;

import Controllers.ConsultMonthlyIncomeController;
import static eapli.util.Console.readInteger;
import java.util.ArrayList;

/**
 *
 * @author João Leal
 * 
 */
public class ConsultMonthlyIncomeUI {
    
    private static final String[] MONTHS={"January","February","March","April","May","June","July","August","September","October","November","December"};
    
    public ConsultMonthlyIncomeUI() {
        
    }
    
    public void consultMonthlyIncome() {
        int year = readInteger("Which year?");
        ConsultMonthlyIncomeController controller = new ConsultMonthlyIncomeController();
        ArrayList<String> pages = controller.ConsultMonthlyIncome(year);
        for (int i = 0; i < pages.size(); i++) {
            String[] aux=pages.get(i).split(";");
            System.out.println(MONTHS[Integer.parseInt(aux[0])-1] + ": " + aux[1]);
        }
    }
}
