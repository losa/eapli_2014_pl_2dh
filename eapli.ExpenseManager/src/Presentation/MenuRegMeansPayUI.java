/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Controllers.MeanPayController;
import Model.MeanPayment;
import Model.TypePayment;
import eapli.util.Console;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Pedro Amaral
 */
public class MenuRegMeansPayUI {

    public MenuRegMeansPayUI() {

    }

    public void show() {
        int op;
        MeanPayController meanPayController = new MeanPayController();
        List<MeanPayment> listMeans = meanPayController.ListAllMeans();
        MenuRegMeansPayUI meansPayUI = new MenuRegMeansPayUI();
        meansPayUI.displayList(listMeans);

        do {
            op = menu();
            switch (op) {
                case 0:
                    System.out.println(" End create means");
                    break;
                case 1:
                    meanPayController.createMeansCash();
                    System.out.println("Mean successfully created");
                    break;
                case 2:
                    createMeansCreditUI();
                    System.out.println("Mean successfully created");
                    break;
                case 3:
                    createMeansDebitUI();
                    System.out.println("Mean successfully created");
                    break;
                case 4:
                    createMeansChequeUI();
                    System.out.println("Mean successfully created");
                    break;
                case 5:
                    listMeans = meanPayController.ListAllMeans();
                    displayList(listMeans);
                    break;
//                case 6:
//                    defcontroller.deleteMeans();
//                    System.out.println("Mean successfully deleted");
//                    break;
                default:
                    System.out.println("Wrong option. Please repeat");
                    break;
            }
        } while (op != 0);
    }

    private int menu() {
        System.out.println("* * *  DEFINE MEANS  * * *\n");
        System.out.println("1. Define cash");
        System.out.println("2. Define credit card");
        System.out.println("3. Define debit card");
        System.out.println("4. Define cheque");
        System.out.println("5. Show Payment means");
        System.out.println("6. Delete Payment Mean");
        System.out.println("0. End define means\n\n");
        int op = Console.readInteger("Choose an option");
        return op;
    }

    public void displayList(List<MeanPayment> listMeans) {
        int i = 0;
        System.out.println("List of Payment Means\n");
        for (MeanPayment meanPayment : listMeans) {
            i = i + 1;
            System.out.println("Mean " + i + "\n" + listMeans);
        }
    }

    public void createMeansCreditUI() {
        MeanPayController controller = new MeanPayController();
        System.out.println("* * *  DEFINE A CREDIT CARD  * * *\n");
        String desc = Console.readLine("Description:");
        int num = Console.readInteger("Number:");
        String bank = Console.readLine("Bank:");
        controller.createMeansCredit(num, desc, bank);
    }

    public void createMeansDebitUI() {
        MeanPayController controller = new MeanPayController();
        System.out.println("* * *  DEFINE A DEBIT CARD  * * *\n");
        String desc = Console.readLine("Description:");
        int num = Console.readInteger("Number:");
        String bank = Console.readLine("Bank:");
        controller.createMeansDebit(num, desc, bank);
    }

    public void createMeansChequeUI() {
        MeanPayController defcontroller = new MeanPayController();
        System.out.println("* * *  DEFINE A CHEQUE * * *\n");
        String desc = Console.readLine("Description:");
        int num = Console.readInteger("Number:");
        String bank = Console.readLine("Bank:");
        defcontroller.createMeansCheque(num, desc, bank);
    }
}