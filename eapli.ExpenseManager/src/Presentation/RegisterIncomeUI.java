/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Controllers.RegisterIncomeController;
import Model.IncomeType;
import persistence.inmemory.MonthPageRepository;
import eapli.util.Console;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Grupo8
 */
public class RegisterIncomeUI {

    /**
     * Shows a menu to gather all the data required to register a new Income
     */
    public void show() {

        RegisterIncomeController registerIncomeController = new RegisterIncomeController();

        Date date = Console.readDate("Data:\n");

        ArrayList<IncomeType> typeIncomes = registerIncomeController.loadListIncomeTypes();
        IncomeType typeIncome = showIncomeTypes(typeIncomes);

        if (typeIncome != null) {
            float incomeValue = Float.parseFloat(Console.readLine("Valor:\n"));
            String description = Console.readLine("Descrição:\n");
            registerIncomeController.registerIncome(incomeValue, description, date, typeIncome);
        }
    }

    /**
     * Shows all the different types of Income
     *
     * @param typeIncomes Arraylist of IncomeTypes
     * @return returns the chosen IncomeType
     */
    public IncomeType showIncomeTypes(ArrayList<IncomeType> typeIncomes) {
        for (int i = 0; i < typeIncomes.size(); i++) {
            System.out.println((i + 1) + " - " + typeIncomes.get(i).toString());
        }
        System.out.println("0 - Voltar");
        int op = Console.readInteger("Escolha Tipo Rendimento:\n");
        if (op > 0 && op <= typeIncomes.size()) {
            return typeIncomes.get(--op);
        } else {
            return null;
        }
    }
}
