/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistence;

import Model.IncomeType;
import java.util.ArrayList;

/**
 *
 * @author Grupo8
 */
public interface IIncomeTypeRepository {
    public void save(IncomeType tr);
    
    public ArrayList<IncomeType> loadAll();
}
