/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistence;

import java.io.FileInputStream;
import java.util.Properties;

/**
 *
 * @author Tiago
 */
public class Factory {
    private static Factory inst = new Factory();
    
    private Factory(){
        try {
            FileInputStream propFile = new FileInputStream("expensemanager.properties");
            Properties p = new Properties(System.getProperties());
            p.load(propFile);
            System.setProperties(p);
        } catch (Exception e) {
            System.setProperty("persistence.repositoryFactory","ExpenseManager.persistence.jpa.JPARepositoryFactory");
        }
        
    }
    
    public IRepositoryFactory getRepositoryFactory(){
        String factoryClass = System.getProperty("persistence.repositoryFactory");
        
        if(factoryClass.equals("ExpenseManager.persistence.jpa.JPARepositoryFactory")){
            return new persistence.jpa.JPARepositoryFactory();
        }else{
            return new persistence.inmemory.InMemoryRepositoryFactory();
        }
    }
    
    public static Factory getInstance(){
        return inst;
    }
}
