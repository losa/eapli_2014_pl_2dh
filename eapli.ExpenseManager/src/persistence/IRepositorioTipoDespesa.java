/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import Model.TipoDespesa;
import persistence.inmemory.RepositorioTipoDespesa;

/**
 *
 * @author Zé
 */
public interface IRepositorioTipoDespesa {

    public void save(TipoDespesa tipoDespesa);
}
