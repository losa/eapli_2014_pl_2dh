/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistence.inmemory;

import Model.MeanPayment;
import java.util.ArrayList;
import persistence.IRepositoryMeanPay;

/**
 *
 * @author Pedro Amaral
 */
public class RepoMeansPayment implements IRepositoryMeanPay{
    private static ArrayList<MeanPayment> repoMeansPay;
    
    @Override
    public ArrayList<MeanPayment> loadAll() {
        return repoMeansPay = new ArrayList();
    }

    @Override
    public void save(MeanPayment mp) {
        repoMeansPay.add(mp);
    }
}
