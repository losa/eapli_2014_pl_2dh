/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.inmemory;

import Model.IncomeType;
import java.util.ArrayList;
import persistence.IIncomeTypeRepository;

/**
 *
 * @author i110300
 */
public class IncomeTypeRepositoryImpl implements IIncomeTypeRepository{

    private static ArrayList<IncomeType> listaTipoRendimentos = new ArrayList<IncomeType>();

    /**
     * Persists an IncomeType inmemory
     * @param tr the IncomeType that is to be persisted inmemory 
     */
    public void save(IncomeType tr) {
        listaTipoRendimentos.add(tr);
    }

    /**
     * Loads all the IncomeTypes
     * @return returns an arrayList with all the IncomeTypes persisted inmemory
     */
    public ArrayList<IncomeType> loadAll() {
        return listaTipoRendimentos;
    }
}
