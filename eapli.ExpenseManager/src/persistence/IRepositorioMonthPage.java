/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package persistence;

import Model.MonthPage;
import java.util.ArrayList;

/**
 *
 * @author JoaoRodrigues
 */
public interface IRepositorioMonthPage {
    void save(MonthPage monthpage);
    ArrayList<MonthPage> loadAll();
}