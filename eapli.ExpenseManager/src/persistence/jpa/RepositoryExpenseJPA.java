/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistence.jpa;

import Model.Expense;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistence.IRepositoryExpense;

/**
 *
 * @author Catarina
 */
public class RepositoryExpenseJPA implements IRepositoryExpense {
       EntityManagerFactory factory =Persistence.createEntityManagerFactory("ExpenseManagerPU");
    EntityManager manager=factory .createEntityManager();
 
    
       @Override
    public void save(Expense expense){
    manager.getTransaction().begin();
    manager.persist(expense);
    manager.getTransaction().commit();
    
    
    }
}
