/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.jpa;

import Model.Book;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import Model.Book;
import persistence.inmemory.MonthPageRepository;
/**
 *
 * @author José
 */
public class BookRepositoryJPA {
      EntityManagerFactory factory =Persistence.createEntityManagerFactory("ExpenseManagerPU");
    EntityManager manager=factory .createEntityManager(); 
    
    public Book load(){
        
     Book livro =new Book();
     manager.getTransaction().begin();
     Query query = manager.createQuery("from Book ex");
     livro=(Book)query.getSingleResult();
     if(livro==null){
     manager.persist(livro);
     }
     manager.close();
     return livro;
    }
}