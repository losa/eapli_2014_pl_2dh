/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.jpa;

import Model.MonthPage;
import persistence.inmemory.MonthPageRepository;
import java.util.ArrayList;
import javax.persistence.*;
import persistence.IRepositorioMonthPage;

/**
 *
 * @author JoaoRodrigues
 */
public class RepositorioMonthPageJPA implements IRepositorioMonthPage {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("ExpenseManagerPU");
    EntityManager manager = factory.createEntityManager();

    @Override
    public void save(MonthPage monthpage) {
        try {
            manager.getTransaction().begin();
            manager.persist(monthpage);
            manager.getTransaction().commit();

        } catch (Exception e) {
            manager.getTransaction().rollback();
        }
    }

    @Override
    public ArrayList<MonthPage> loadAll() {
        ArrayList<MonthPage> list = new ArrayList<>();
        manager.getTransaction().begin();
        Query query = manager.createQuery("select i from MonthPage i", MonthPageRepository.class);
        list.addAll(query.getResultList());
        manager.close();

        return list;
    }
}