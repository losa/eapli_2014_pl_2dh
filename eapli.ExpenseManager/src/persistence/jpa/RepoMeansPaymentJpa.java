/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistence.jpa;

import Model.MeanPayment;
import Model.MonthPage;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import persistence.IRepositoryMeanPay;
import persistence.inmemory.MonthPageRepository;

/**
 *
 * @author byic
 */
class RepoMeansPaymentJpa implements IRepositoryMeanPay {

    EntityManagerFactory factory =Persistence.createEntityManagerFactory("ExpenseManagerPU");
    EntityManager manager=factory .createEntityManager();
    
    public RepoMeansPaymentJpa() {
    }

    @Override
    public void save(MeanPayment mp) {
        manager.getTransaction().begin();
        manager.persist(mp);
        manager.getTransaction().commit();
    }

    @Override
    public List<MeanPayment> loadAll() {
        ArrayList<MeanPayment> list = new ArrayList<>();
        manager.getTransaction().begin();
        Query query = manager.createQuery("select i from MeanPayment i", MeanPayment.class);
        list.addAll(query.getResultList());
        manager.close();

        return list;
    }
    
}