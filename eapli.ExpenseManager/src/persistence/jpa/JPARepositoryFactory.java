/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.jpa;

import persistence.IIncomeTypeRepository;
import persistence.IRepositorioMonthPage;
import persistence.IRepositorioTipoDespesa;
import persistence.IRepositoryFactory;
import persistence.IRepositoryMeanPay;
import persistence.inmemory.IncomeTypeRepositoryImpl;
import persistence.inmemory.MonthPageRepository;
import persistence.inmemory.RepoMeansPayment;

/**
 *
 * @author Tiago
 */
public class JPARepositoryFactory implements IRepositoryFactory {

    public RepositorioIncomeJPA getIncomeJPARepository() {
        return new RepositorioIncomeJPA();
    }

    public RepositorioTipoDespesaJPA getTipoDespesaJPARepository() {
        return new RepositorioTipoDespesaJPA();
    }

    @Override
    public IIncomeTypeRepository getIncomeTypeRepository() {
        return new IncomeTypeRepositoryJPAImpl();
    }

    @Override
    public IRepositoryMeanPay getRepositoryMeanPay() {
        return new RepoMeansPayment();
    }

    @Override
    public IRepositorioMonthPage getMonthPageRepository() {
        return new RepositorioMonthPageJPA();
    }

    @Override
    public IRepositorioTipoDespesa getRepositorioTipoDespesa() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
