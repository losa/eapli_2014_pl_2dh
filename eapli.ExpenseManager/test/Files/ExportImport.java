package Files;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import static Factories.RepositorioFactory.getBookRepository;
import Model.Book;
import Model.Expense;
import Model.Income;
import Model.MonthPage;
import Model.IncomeType;
import java.util.Calendar;
import java.util.Date;
import persistence.inmemory.RepositorioLivro;
import Controllers.FilesController;

import Factories.RepositorioFactory;
import Model.TipoDespesa;

import Presentation.FileExportImportUI;

/**
 *
 * @author Dr4k
 */
public class ExportImport {

    //teste para data 2012 fevereiro
     public static boolean verifyMonthPageDate(Date start,Date end) {
        Calendar start_c = Calendar.getInstance();
        Calendar end_c =Calendar.getInstance();
        start_c.setTimeInMillis(start.getTime());
        end_c.setTimeInMillis(end.getTime());
        Calendar aux=Calendar.getInstance();
        aux.setTimeInMillis((new Date(2012,2,0)).getTime());
        if(start_c.getTimeInMillis()<=aux.getTimeInMillis() && aux.getTimeInMillis()<=end_c.getTimeInMillis()){
        return true;
        }else{return false;}
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       
        RepositorioLivro livro=RepositorioFactory.getBookRepository();
         Book livro1 = livro.loadBook();
         
         
         MonthPage  pagina=new MonthPage(1991,10);
         
         
         for(int i=0;i<27;i++){
         pagina.addIncome(new Income(i,"pagina1",new Date(1991, 10, i+1),new IncomeType("tipo rendimento")));
         }
         for(int i=0;i<27;i++){


        pagina.registerExpense(new Expense(new Date(1991, 10, i+1), i,"uma despesa",new TipoDespesa("Tipo despesa")));

         }
         
         livro1.saveMonthPage(pagina);
         
         
         
         MonthPage  pagina1=new MonthPage(1991,11);
         
         
         for(int i=0;i<27;i++){
         pagina1.addIncome(new Income(i,"pagina2",new Date(1991, 11, i+1),new IncomeType("tipo rendimento")));
         }
         for(int i=0;i<27;i++){
        pagina1.registerExpense(new Expense(new Date(1991, 11, i+1), i,"uma despesa",new TipoDespesa("Tipo despesa")));

         }
         
         livro1.saveMonthPage(pagina1);
         
         System.out.println("Numero de paginas no livro:"+livro1.getListMonthPages().size());
         
         
        FilesController cont= new FilesController();
         cont.ExportCSV("teste.csv", FileExportImportUI.movement_type.All,new Date(1991,10,1),new Date(1991,11,31));

         
        
         //cont.ImportCSV("teste.csv");
         
         
         
                
    }
}
